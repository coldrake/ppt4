package ranges;

import java.util.ArrayList;


public class OverlappingRanges {

	/**
	 * Returns the smallest integer that is present in the maximum number of
	 * input ranges
	 * 
	 * @param startPoints
	 *            the list of starting points for the ranges
	 * @param endPoints
	 *            the list of end points for the ranges
	 * @return the smallest integer that is present in the maximum number of
	 *         ranges
	 * @throws NoOverlapException
	 *             when there is no overlap between any of the ranges
	 */
	public static Integer maxOverlapInt(ArrayList<Integer> startPoints,
			ArrayList<Integer> endPoints) throws NoOverlapException {
		// TODO: Implement this method
		Integer maxOverlap = 0;
		int[] rangeArray = new int[1000]; //ranges must have a min of 0 and a max of 999 since I couldn't figure out another way
		//index of the rangeArray is the digit from 0-999, and the int at that index value is the frequency
		int mostFrequent = 0;
		
		if(startPoints.size() != endPoints.size()){		//If we're given a messed up set of ranges, 
			return maxOverlap;							//we assume user error and essentially end the program
		}
		
		for(int i = 0; i < startPoints.size(); i++){		//Go through each range within the set
			if(startPoints.get(i) < endPoints.get(i)){		//Ignore any backwards ranges
					for(int m = startPoints.get(i); m <= endPoints.get(i); m++){	//go through each int within the range, 
					rangeArray[m]++;												//adding to its frequency in rangeArray[]
				}
			}
		}
			
		for(int b = 1; b < 1000; b++){			//find the most frequent integer, looking from the bottom up
			if(rangeArray[b] > rangeArray[b-1]){
				mostFrequent = b;
			}
		}
		
		maxOverlap = rangeArray[mostFrequent];	//set maxOverlap equal to the most frequent int
		return maxOverlap;
	}

}
