package erasures;

import static org.junit.Assert.*;

import org.junit.Test;

public class SingleErasureTest {

	@Test
	public void test1() {
		String s1 = new String("rubber");
		String s2 = new String("uber");
		assertEquals(SingleErasure.testSingleErasure(s1, s2), false);
	}

	@Test
	public void test2() {
		String s1 = new String("ubc");
		String s2 = new String("bc");
		assertEquals(SingleErasure.testSingleErasure(s1, s2), true);
	}

	@Test
	public void test3() {
		String s1 = new String("ubc");
		String s2 = new String("uc");
		assertEquals(SingleErasure.testSingleErasure(s1, s2), true);
	}

	@Test
	public void test4() {
		String s1 = new String("cuba!");
		String s2 = new String("cua");
		assertEquals(SingleErasure.testSingleErasure(s1, s2), false);
	}
	
	@Test
	public void test5() {
		String s1 = new String("train");
		String s2 = new String("Rain");
		assertEquals(SingleErasure.testSingleErasure(s1, s2), false);
	}

	@Test
	public void test6() {
		String s1 = new String("train");
		String s2 = new String("rain");
		assertEquals(SingleErasure.testSingleErasure(s1, s2), true);
	}
	
}
