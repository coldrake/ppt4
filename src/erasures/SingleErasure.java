package erasures;

public class SingleErasure {

	/**
	 * Test if s2 can be created by erasing exactly one character from s1.
	 * 
	 * @param s1
	 *            the first string, which is the "original" string
	 * @param s2
	 *            the string that may have been created by deleting exactly one
	 *            character from the "original" string
	 * @return true if s2 can be created by deleting exactly one character from
	 *         s1 and false otherwise.
	 */
	public static boolean testSingleErasure(String s1, String s2) {
		// TODO: Implement this method; the code provided here is a stub only.
		boolean same = false;
		int i;	//our for loop index
		int m = 0;	//our "missed search" index, or the index of the first char mismatch
		String org = s1; //The "original" string
		String mod = s2; //The "modified" string
		
		//We check that the lengths are one different
 		if(mod.length() != ((org.length())-1)){
			return false;
 		}	
		//Then we convert Strings to Char arrays
		char[] orgStr = org.toCharArray();
		char[] modStr = mod.toCharArray();
	
 		//Go through the two char arrays to find the index of the mismatch
		
 		for(i = 0; i < org.length(); i++){
 			if(orgStr[i] != modStr[i]){
 				m = i;  //Set M to index of the mismatch
 				break;
 			}
 		}
 		if( m==0 ){			//if the mismatch is the first letter...
 			String orgSubStr1 = org.substring(1);
 			String modSubStr1 = mod.substring(0);
 			if(orgSubStr1.equals(modSubStr1)){
 	 			same = true;
 	 		}
 		}
 		else{ 				//Otherwise do a double calculation of the above
 			String orgSubStr1 = org.substring(0,m-1);
 			String orgSubStr2 = org.substring(m+1);
 			String modSubStr1 = mod.substring(0,m-1);
 			String modSubStr2 = mod.substring(m);
 			if(orgSubStr1.equals(modSubStr1) && orgSubStr2.equals(modSubStr2)){
 				same = true;
 			}
 		}
 		
		return same;
	}

}
