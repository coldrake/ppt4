Programming Proficiency Test
=========

February 28, 2015

## General Instructions

+ There are only two questions that you need to complete.
+ You have 130 minutes (2h 10m) to complete the tasks.
+ You will start at 9:45 a.m. and finish before 11:55 a.m.
+ We will start cloning repositories at **12 noon** so you should have committed all your work to BitBucket by that time (see submission instructions below).
+ Take your time to read the questions. 
+ Skeleton code can be obtained by cloning this repository. Add JUnit to your build path in Eclipse.
+ Best of luck!

## Submission Instructions

+ Create a BitBucket repository called `PPT4`. You should commit all your work to this repository. 
+ **Make sure you provide read+write access to user id `gsathish`.**
+ **No grace for late submissions and/or not providing access in time to the instructor.**
+ Do not wait until the last minute to push your work to BitBucket. It is a good idea to push your work at intermediate points as well.

## Question 1: Erasure Coding

Thor is working with a `String` `s1`. Loki, the mischievous creature that he is, deletes/erases one character from `s1` and creates String `s2`. (Thank Asgard for immutable Strings!)

Given two `String`s, `s1` and `s2`, can you determine if `s2` could have been created by deleting exactly one character from `s1`. You will implement a method that takes `s1` and `s2` as arguments and returns either `true` or `false`.
	 
#### Examples
1. `s1` = “rubber” and `s2` = “uber”. The method should return `false`.
1. `s1` = “ubc” and `s2` = “bc”. The method should return `true`.
1. `s1` = “ubc” and `s2` = “uc”. The method should return `true`.
1. `s1` = “cuba!” and `s2` = “cua”. The method should return `false`.
1. `s1` = “train” and `s2` = “Rain”. The method should return `false`.
1. `s1` = “train” and `s2` = “rain”. The method should return `true`.

## Question 2: Overlapping Ranges

Suppose you want to devise a method to schedule a meeting between many friends tomorrow. Each of them provides a time range for when they are available (in 24-hour format). A suitable meeting time would when a majority of time ranges overlap.

To make the problem a bit more precise and a bit more general, suppose a range is on the non-negative number line with each endpoint being an integer, and the first endpoint being smaller than the other. 

+ `5` to `12` is a valid range. 
+ `-10` to `20` is not a valid range because one endpoint is negative. 
+ `10` to `33.6` is not a valid range because one of the endpoints is not an integer. 
+ `32` to `11` is not a valid range because the first endpoint should be smaller than the second endpoint.
+ `11` to `11` is not a valid range because the first endpoint is not smaller than the second endpoint (this condition also eliminates empty ranges).

The problem to solve is as follows: Given `N` ranges, find the *smallest integer* that is included in the maximum number of ranges.

#### Examples

1. Range1 is 1 to 6. Range2 is 2 to 5. Range3 is 3 to 4. Range4 is 7 to 10. In this example, 3 is the smallest integer present in a maximum number of ranges.
1. Range1 is 2 to 5. Range2 is 1 to 10. In this example, 2 is the smallest integer present in the maximum number of ranges.
1. Range1 is 10 to 15. Range2 is 1 to 20. Range3 is 11 to 25. Range4 is 0 to 2. In this example, 11 is the smallest integer present in the maximum number of ranges.

#### Notes

1. The ranges will be provided as arguments to the method to implement using two `ArrayList`s. The first `ArrayList` will contain the starting points of all the ranges. The second `ArrayList` will contain the end points of all the ranges. The i-th entry in each `ArrayList` corresponds to the i-th range.
1. Improperly constructed or invalid ranges should be ignored.
1. If the ranges do not overlap then a `NoOverlapException` should be thrown.

## What Should You Implement / Guidelines

+ You should implement all the methods that are indicated with `TODO`.
+ Passing the provided tests is the minimum requirement. Use the tests to identify cases that need to be handled. Passing the provided tests is *not sufficient* to infer that your implementation is complete and that you will get full credit. Additional tests will be used to evaluate your work. The provided tests are to guide you.
+ You can implement additional helper methods if you need to but you should keep these methods `private` to the appropriate classes.
+ You do not need to implement new classes.
+ You can use additional standard Java libraries by importing them.
+ Do not throw new exceptions unless the specification for the method permits exceptions.# PPT4
